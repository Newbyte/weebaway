"use strict";

const words = [ "Anime", "AMV", "OST", "」", "Sakurasou", "ore", "wa", "Yahari", "na", "Shigatsu", "sheishun" ];
let onWatchPage = false;
let rickRolling = false;
console.log("hi");
setTimeout(init, 3000);

function init() {
    const frontPageWatcherNode = document.getElementsByTagName("ytd-app")[0];
    const frontPageWatcherConfig = {
        attributes: true
    };
    console.log(frontPageWatcherNode);
    
    const observer = new MutationObserver(frontPageWatcherFunc);
    observer.observe(frontPageWatcherNode, frontPageWatcherConfig);
}

function frontPageWatcherFunc(mutationList) {
    console.log("hi");
    console.log(mutationList);

    for (let i = 0; i < mutationList.length; i++) {
        console.log("hello!!");
        if (mutationList[i].attributeName === "is-watch-page") {
            onWatchPage = true;
            console.log("On watch page");
            setTimeout(function() {
                if (checkWords()) {
                    console.log("weeb video detected");
                    rickRollPayload();
                }
            }, 3000);
        }
    }
}

const config = {
    attributes: true,
    chidList: true,
    subtree: true
};

function checkWords() {
    console.log("checking words ... ");
    const title = document.getElementsByTagName("title")[0];
    const titleText = title.innerHTML;
    
    for (let i = 0; i < words.length; i++) {
        if (titleText.indexOf(words[i]) !== -1) {
            console.log(words[i]);
            return true;
        }
    }

    return false;
}

function callback(mutationsList, observer) {
    console.log(mutationsList);
    console.log(observer);
   
    if (!rickRolling)
        setTimeout(rickRollPayload, 5000);
}

function rickRollPayload() {
    rickRolling = true;
    const container = document.getElementById("movie_player");
    const video = document.getElementsByClassName("video-stream html5-main-video");

    for (let i = 0; i < video.length; i++) {
        video[i].pause();
    }

    container.innerHTML = null;

    const customIframe = document.createElement("IFRAME");
    customIframe.onload = function() {
        setTimeout(function() {
            const rickRollDocument = customIframe.contentDocument || customIframe.contentWindow.document;
            const rickRollButton = rickRollDocument.getElementsByClassName("ytp-large-play-button ytp-button");

            for (let i = 0; i < rickRollButton.length; i++) {
                eventFire(rickRollButton[i], "click");
            }
        }, 0);
    }

    customIframe.setAttribute("frameborder", "0");
    customIframe.setAttribute("style", "height: 100%; width: 100%");
    customIframe.setAttribute("id", "rick-roll-container");
    customIframe.setAttribute("class", "html5-video-container");
    customIframe.setAttribute("allow", "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture");
    customIframe.src = "https://www.youtube.com/embed/dQw4w9WgXcQ";

    container.appendChild(customIframe);
}

function eventFire(el, etype){
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    var evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}

const observer = new MutationObserver(callback);
observer.observe(frontPageWatcherNode, frontPageWatcherConfig);
